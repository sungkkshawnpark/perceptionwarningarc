﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ALPerception;

public class Npc : MonoBehaviour
{
    [SerializeField]
    private GameObject player;

    private ALPerceptionCtrl pc;

    private void Awake()
    {
        pc = GetComponent<ALPerceptionCtrl>();
    }

    // Use this for initialization
    void Start()
    {
        pc.Init(gameObject, 90, player);

        pc.OnLoad();

        pc.ShowPerceptionSight();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
