﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ALPerception
{

    public class ALPerceptionSight : MonoBehaviour
    {
        public const string RESOURCE_ETC_PERCEPTION_SIGHT = "PerceptionSight";
        public const float HEIGHT_PERCEPTIONSIGHT = 0.0f;   // NPC의 은신 감지 시야가 표시되는 높이.

        public float Range
        {
            get
            {
                return _range;
            }
            set
            {
                _range = value;

                MeshUpdate();
            }
        }
        [SerializeField]
        private float _range;

        public int Angle
        {
            get
            {
                return _angle;
            }
        }
        [SerializeField]
        private int _angle;

        public int angleBlade = 10;     // 부채살의 각격을 최대 각도. 이 값이 작으면 부채살이 촘촘해진다.

        private int numBlades;

        [SerializeField]
        private Mesh _mesh;

        [SerializeField]
        private MeshFilter _meshFilter = null;

        public void Init(int angle, float range)
        {
            this._angle = angle;
            this._range = range;
        }

        public void UpdateAlertRange(float range)
        {
            _range = range;
        }

        public static ALPerceptionSight Create(GameObject parent)
        {
            var resource = Resources.Load<GameObject>(RESOURCE_ETC_PERCEPTION_SIGHT);

            var position = parent.transform.position;
            position.y += HEIGHT_PERCEPTIONSIGHT;

            var go = GameObject.Instantiate(resource, position, parent.transform.rotation);
            go.transform.parent = parent.transform;

            go.SetActive(false);

            var ps = go.GetComponent<ALPerceptionSight>();
            //ALLogger.Assert(ps != null, "[PerceptionCtrl], it is not perception sight, object:{0}, sight:{1}", parent, go);

            return ps;
        }

        private static readonly int[] TRIANGLE_VALUES = new int[]
        {
            0, 5, 1,
            0, 4, 5,
            1, 5, 6,
            1, 6, 2,
            2, 6, 7,
            2, 7, 3,
        };

        private void MeshSetup(int angle, out Vector3[] vertices, out Vector2[] uvs, out int[] triangles)
        {
            numBlades = (angle + angleBlade - 1) / angleBlade;  // 부채살 몇개로 할 건지 결정
            float angleBladeReal = (float)angle / numBlades;
            /*
                    listVertices = new List<Vector3> ();
                    listVertices.Add (new Vector3 (0, 0, 0));

                    listUV = new List<Vector2> ();
                    listUV.Add (new Vector2 (0, 0));

                    listTriangles = new List<int> ();

                    for (int i = 0; i <= numBlades; ++i) {
                        float radian = (i * angleBladeReal - angle / 2.0f) * Mathf.PI / 180.0f;

                        listVertices.Add (new Vector3 (range * Mathf.Sin(radian), 0, range * Mathf.Cos(radian)));
                        listUV.Add (new Vector2 (0, 1));
                    }

                    for (int i = 0; i < numBlades; ++i) {
                        listTriangles.Add (0);
                        listTriangles.Add (i+1);
                        listTriangles.Add (i+2);
                    }
            */
            vertices = new Vector3[(numBlades + 1) * 4];
            uvs = new Vector2[(numBlades + 1) * 4];
            triangles = new int[numBlades * TRIANGLE_VALUES.Length];

            for (int i = 0; i <= numBlades; i++)
            {
                float radian = (i * angleBladeReal - angle / 2.0f) * Mathf.PI / 180.0f;

                vertices[i * 4 + 0] = (new Vector3(_range * Mathf.Sin(radian), 0, _range * Mathf.Cos(radian)));
                vertices[i * 4 + 1] = (new Vector3(_range * 0.75f * Mathf.Sin(radian), 0, _range * 0.75f * Mathf.Cos(radian)));
                vertices[i * 4 + 2] = (new Vector3(_range * 0.35f * Mathf.Sin(radian), 0, _range * 0.35f * Mathf.Cos(radian)));
                vertices[i * 4 + 3] = (new Vector3(_range * 0.1f * Mathf.Sin(radian), 0, _range * 0.1f * Mathf.Cos(radian)));

                if (i == 0 || i == numBlades)
                {
                    uvs[i * 4 + 0] = (new Vector2(1, 1));
                    uvs[i * 4 + 1] = (new Vector2(1, 0));
                    uvs[i * 4 + 2] = (new Vector2(1, 0));
                    uvs[i * 4 + 3] = (new Vector2(1, 1));
                }
                else
                {
                    uvs[i * 4 + 0] = (new Vector2(0, 1));
                    uvs[i * 4 + 1] = (new Vector2(0, 0));
                    uvs[i * 4 + 2] = (new Vector2(0, 0));
                    uvs[i * 4 + 3] = (new Vector2(0, 1));
                }
            }

            for (int i = 0; i < numBlades; i++)
            {

                for (int j = 0; j < TRIANGLE_VALUES.Length; j++)
                {
                    triangles[i * TRIANGLE_VALUES.Length + j] = 4 * i + TRIANGLE_VALUES[j];
                }
            }
        }

        private void Start()
        {
            MeshUpdate();
        }

        public void MeshUpdate()
        {
            _mesh = new Mesh();
            //ALLogger.Assert(_meshFilter);
            _meshFilter.mesh = _mesh;

            Vector3[] vertices;
            Vector2[] uvs;
            int[] triangles;

            MeshSetup(_angle, out vertices, out uvs, out triangles);
            _mesh.vertices = vertices;
            _mesh.uv = uvs;
            _mesh.triangles = triangles;
        }
    }
}