﻿//=============================================================================
//
//	ALPerceptionCtrl.cs
//	Albatross Client
//	NPC의 은신감지 처리를 하는 컨트롤러
//
// 	초기화 시 Diabled 되어있다. 은신감지 메니저(PerceptionMgr)가 상태를 파악해서 enable해주면 작동을 시작한다.
//	
//
//	Created by Joohyoung Park on 2015-07-27
//	Copyright 2015 Maverick Games. All rights reserved.
//
//=============================================================================

using UnityEngine;
using System;
using System.Text;
using System.Collections.Generic;
using DG.Tweening;

namespace ALPerception
{
    public interface IPerceptionCtrl
    {
        bool IsOn { get; }

        void Init(GameObject owner, float ownerPerceptionAngle, GameObject player);

        void ShowPerceptionSight();
        void HidePerceptionSight();

        //void OnLoad(int ownerSkillLevelPerception, int playerSkillLevelPerception);

        //bool IsPlayerDetected();
    }

    public class ALPerceptionCtrl : MonoBehaviour, IPerceptionCtrl
    {
        public const float MIN_PERCEPTION_RANGE = 0.5f;  // 은신 감지 시야의 최소 거리
        public const float MIN_PERCEPTION_ANGLE = 10f;      // 은신 감지 시야의 최소 각도
        public const float MIN_PERCEPTION_ANGLE_HALF = MIN_PERCEPTION_ANGLE * 0.5f;

        public float AlertSpeed = 1f;

        public float PerceptionRange = 3;

        [SerializeField]
        private bool _isPlayerDetected = false;

        [Range(0f, 5f)]
        public float PerceptionDistanceMargin = 0.75f;

        [Range(0f, 180f)]
        public float PerceptionAngleMargin = 30f;

        public float HalfPerceptionAngleMargin
        {
            get
            {
                return PerceptionAngleMargin * 0.5f;
            }
        }

        public bool IsOn
        {
            get
            {
                return _isOn;
            }
        }
        [SerializeField]
        private bool _isOn = false;

        [SerializeField]
        private float _perceptionRange;   // 동적인 데이터

        [SerializeField]
        private int _angle;     // 정적인 데이터

        [SerializeField]
        private float _halfAngle;

        private GameObject _owner;
        private GameObject _player;
        private float _playerRadius;    // 플레이어 Colider의 반경.

        private bool _isLoaded = false;

        [SerializeField]
        private ALPerceptionSight _ps;

        [SerializeField]
        private ALAlertSight _as;

        /*
         * 
         * [시야의 반지름] = ROOT ( 1 / Pi * ( 1 - [은신 기술 숙련도에 따른 시야 수정치]
         * - [조명에 따른 시야 수정치]
         * - [시간대에 따른 시야 수정치] ) * [NPC의 시야 각도] / 360 / [NPC의 탐지 숙련도에 따른 시야 면적] )
         */

        public Ease ease;

        public void Init(GameObject owner, float perceptionAngle, GameObject player)
        {
            _owner = owner;
            //ALLogger.Assert(_owner != null, "[PerceptionCtrl], this object is not npc, object:{0}", this);

            _angle = Mathf.FloorToInt(perceptionAngle);
            //ALLogger.Assert(_angle > 0);

            _halfAngle = _angle * 0.5f;
            //ALLogger.Assert(_halfAngle > 0);

            _player = player;
            //ALLogger.Assert(_player);

            var cc = _player.GetComponent<CharacterController>();
            _playerRadius = cc.radius;
            //ALLogger.Assert(_playerRadius > 0);
        }

        //public void UpdatePerceptionRange(float originRange, float correctionRatio)
        //{
        //    //var originRange = EvaluatePerceptionRange(_owner.NpcInfo.SkillLevelPerception, _player.Property.GetSkillLevel(ALEnum.SkillType.SNEAK));
        //    var resultRange = originRange * correctionRatio;

        //    _range = resultRange;

        //    if (_ps == null || _ps.gameObject == null)
        //    {
        //        return;
        //    }

        //    _ps.Range = resultRange;
        //}

        //private float EvaluatePerceptionRange(int npcPerceptionLvl, int playerSneakLvl)
        //{
        //    // 플레이어의 은신 스킬 레벨이 0 이라면 NPC 인지 범위 배열 인덱스를 벗어나므로,
        //    // 해당 레벨로서 0 이 인자로 들어오면 이를 인자의 최소 요구값인 1 로 맞춰준다.
        //    if (playerSneakLvl == 0)
        //    {
        //        playerSneakLvl = 1;
        //    }

        //    //ALLogger.Assert(0 < playerSneakLvl && playerSneakLvl <= 10);
        //    //ALLogger.Assert(0 <= npcPerceptionLvl && npcPerceptionLvl <= 10);

        //    // NPC Perception Level to string
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("perceptionLevel");
        //    sb.AppendFormat(npcPerceptionLvl.ToString("D2"));

        //    // NPC의 탐지 스킬 레벨에 따른 탐지 범위 배열을 얻는다
        //    //var arPerceptionRange = ALData.Static.Find<ALData.PerceptionRangeTblData>(sb.ToString());
        //    var arPerceptionRange = new int[] { 1, 2, 3, 4, 5 };

        //    // 플레이어의 은신 스킬을 보정한 탐지 범위 값을 돌려준다
        //    return arPerceptionRange[playerSneakLvl - 1];
        //}

        [SerializeField]
        private float _alertRange = 0;

        private float tempSpeed;

        public float easeTime;

        void Update()
        {
            if (_player == null)
            {
                return;
            }

            if (_isOn == false)
            {
                return;
            }

            if (IsPlayerInRange(_perceptionRange))
            {
                // TODO: FSM으로 구현할 것
                _as.gameObject.SetActive(true);

                if (_alertRange <= _perceptionRange)
                {
                    DOTween.To(() => tempSpeed, x => tempSpeed = x, AlertSpeed, easeTime).SetEase(ease);
                    Debug.LogErrorFormat("{0}", tempSpeed);
                    
                    _alertRange += Math.Min(tempSpeed, AlertSpeed);
                }

                // 플레이어 발견 이벤트
                _isPlayerDetected = IsPlayerInRange(_perceptionRange) && (_alertRange >= _perceptionRange);
                if (_isPlayerDetected)
                {
                    OnPlayerDetected();
                }
            }
            else
            {
                DOTween.Clear();
                tempSpeed = 0;

                // TODO: FSM으로 구현할 것
                _as.gameObject.SetActive(true);

                if (_alertRange > MIN_PERCEPTION_RANGE)
                {
                    _alertRange -= AlertSpeed;
                }
            }

            if (_as.Range != _alertRange)
            {
                _as.UpdateAlertRange(Mathf.Max(MIN_PERCEPTION_RANGE, _alertRange));

                _as.MeshUpdate();
            }

            // 경계 범위 (Warning Range)만 줄인다
            // 결국, 범위는 2가지 경우로 분류할 수 있다.
            // 1. 표시 범위
            // 2. 경계 범위 (실제 상대가 경계 범위에 닿으면 작동)

            // 아니면, 애초에 은신 시야를 표시 안 하다가
            // 은신 시야에 들어오면 (이건 경계 범위)가 되겠고
            // 실제 감지하게 되는 (감지 범위)가 서서히 늘어난다 (1초)
            // 그리고 줄어드는 속도는 감지 범위가 늘어나는 속도의 1/3.
            // (요구 사항은 이 경우에 해당한다.

            // TODO: 그렇다면 할 것은
            // 1. 플레이어 감지 (Player Detected) 라는 조건이 충족되는 경우
            // 바로 플레이어를 감지하게 하는 것이 아니라,
            // 유예 기간 (감지 범위가 늘어나는)을 두는 것.

            // 2. 감지 범위를 두 가지 종류를 둔다.
            //  1) 보이는 감지 범위 (Sense Range) 가 있다 (결국 두 개를 동시에 그려야 한다는 건데 overhead를 예상).
            // -> 이 범위는 기존의 은신 시야와 그 형태나 크기가 정확히 일치함.
            // 감지 범위는 일종의 유예 구간으로서, 들어가도 바로 안 걸림.
            // 대신, 이 범위에 닿는 순간부터 경계 범위가 활성화 (activated) -> 0 % to 100 % 까지 증가

            // 이 범위 안에 들어오면, 플레이어가 육안으로 확인할 수 있는 경계 범위 (Alert Range)
            // 경계 범위에 닿아도 됨.
            // 그러나 경게 범위의 면적이 100%가 되는 순간
            // -> 경계 범위에때리든 죽이든 도망가든 알아서 함
        }

        /// <summary>
        /// 은신감지 시야에 들었다 (플레이어가 발각됐다)
        /// </summary>
        private void OnPlayerDetected()
        {
            //_player.adEffectContainer.Unset(ALEnum.EffectType.SNEAK);

            //_player.SendNotification(GameConfig.AiNoti.TargetLost, _owner.gameObject);
            //_player.Room.SendNotification2Npc(GameConfig.AiNoti.Perception, _player.gameObject);

            //Destroy(_player.gameObject);

            //Debug.LogError("플레이어 발견됨");
        }

        //-------------------------------------------------------------------------

        public void ShowPerceptionSight()
        {
            //if (_owner == null || gameObject.IsDestroyed())
            //{
            //    ALLogger.Warning("[PerceptionCtrl], owner is null or destroyed, name:{0}", _owner);
            //    return;
            //}

            //if (_ps == null || _ps.gameObject == null || _ps.gameObject.IsDestroyed())
            //{
            //    ALLogger.Warning("[PerceptionCtrl], gameObject is null or destroyed, name:{0}", _owner);
            //    return;
            //}

            //if (_owner.condition.IsSet(ALEnum.ConditionType.CAN_NOT_PERCEPT))
            //{
            //    return;
            //}

            //if (_isOn == true)
            //{
            //    ALLogger.Warning("[{0}].DisplaySight is already on", _owner);
            //    return;
            //}

            _ps.gameObject.SetActive(true);

            //_as.gameObject.SetActive(true);

            _isOn = true;
        }

        public void HidePerceptionSight()
        {
            //if (_owner == null || gameObject.IsDestroyed())
            //{
            //    ALLogger.Warning("[PerceptionCtrl], owner is null or destroyed, name:{0}", _owner);
            //    return;
            //}

            //if (_ps == null || _ps.gameObject == null || _ps.gameObject.IsDestroyed())
            //{
            //    ALLogger.Warning("[PerceptionCtrl], gameObject is null or destroyed, name:{0}", _owner);
            //    return;
            //}

            //if (_isOn == false)
            //{
            //    ALLogger.Warning("[{0}].DisplaySight is already off", _owner);
            //    return;
            //}

            _as.gameObject.SetActive(false);

            _ps.gameObject.SetActive(false);

            _isOn = false;
        }

        //=========================================================================
        // 은신감지 시야에 Player가 포함되는 지 체크

        public bool IsPlayerInRange(float range)
        {
            // 2차원 계산을 한다
            Vector2 playerPos = new Vector2(_player.transform.position.x, _player.transform.position.z);
            Vector2 npcPos = new Vector2(transform.position.x, transform.position.z);
            float distance = Vector2.Distance(playerPos, npcPos);

            // 거리 체크
            var pd = Mathf.Max(range + _playerRadius - PerceptionDistanceMargin, MIN_PERCEPTION_RANGE);
            if (distance > pd)
            {
                // 체크 범위 밖
                return false;
            }

            var pa = transform.rotation.eulerAngles.y;
            float globalAngle = pa;
            float halfRelativeAngle = Mathf.Max(_halfAngle - HalfPerceptionAngleMargin, MIN_PERCEPTION_ANGLE_HALF);
            float leftAngle = globalAngle - halfRelativeAngle;
            float rightAngle = globalAngle + halfRelativeAngle;

            // a x + b y = distance
            Vector2 offset = playerPos - npcPos;
            float dl = GetDist(leftAngle + 90.0f, offset);  // 왼쪽 시야선과의 거리 (+면 안쪽, -면 밖)
            float dr = GetDist(rightAngle - 90.0f, offset); // 오른쪽 시야선과의 거리 (+면 안쪽, -면 밖)

            if (dl > 0 && dr > 0)
            {
                // 양쪽 시야선 모두 안쪽
                return true;
            }
            else if (dl * dr < 0)
            {
                // 한쪽만 안쪽

                if (_angle >= 180f)
                {
                    // 180도가 넘어가면 한쪽만 들어가도 포함된다.
                    return true;
                }
                else if (dl > 0)
                {
                    // 왼쪽 시야선 안쪽
                    return -dr < _playerRadius;
                }
                else
                {
                    // 오른쪽 시야선 안쪽
                    return -dl < _playerRadius;
                }
            }

            // 양쪽 시야선 밖
            return false;
        }

        // angle = line의 법선각도.
        // pos = 거리를 측정할 위치 (NPC위치에서 Player의 상대위치)
        private float GetDist(float angle, Vector2 pos)
        {
            float radian = angle * Mathf.PI / 180.0f;
            float s = Mathf.Sin(radian);
            float c = Mathf.Cos(radian);

            return s * pos.x + c * pos.y;   // 은신감지 시야 line과 거리계산
        }

        ////=========================================================================
        //// IMPLEMENT IALReloader
        //#region IALReloader
        //public bool IsLoaded()
        //{
        //    return _isLoaded;
        //}

        //public void Load()
        //{
        //    if (!ALPerceptionMgr.Instance.Contains(this))
        //    {
        //        ALPerceptionMgr.Instance.AddPerceptionCtrl(this);
        //    }

        //    _isLoaded = true;

        //    OnLoad(_owner.NpcInfo.SkillLevelPerception, _player.Property.GetSkillLevel(ALEnum.SkillType.SNEAK));
        //}

        //public void AfterLoad()
        //{
        //}

        public void OnLoad()
        {
            if (_ps == null || _ps.gameObject == null)
            {
                _ps = ALPerceptionSight.Create(gameObject);
            }

            // Recalculate perception range because it can change anytime
            _perceptionRange = PerceptionRange;

            //ALLogger.Assert(_ps != null);
            _ps.Init(_angle, _perceptionRange);

            if (_as == null || _as.gameObject == null)
            {
                _as = ALAlertSight.Create(gameObject);
            }

            _alertRange = MIN_PERCEPTION_RANGE;

            _as.Init(_angle, _alertRange);
        }

        //public void Unload()
        //{
        //    ALPerceptionMgr.Instance.RemovePerceptionCtrl(this);

        //    _isLoaded = false;
        //}

        //public ALAI.Action.Base GetCurAction()
        //{
        //    return null;
        //}

        //public List<Collider> Colliders
        //{
        //    get
        //    {
        //        return null;
        //    }
        //}
        //#endregion
    }
}