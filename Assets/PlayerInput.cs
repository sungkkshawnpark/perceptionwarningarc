﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public float PlayerSpeed = 0.5f;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        var o = gameObject.transform.position;
        var oldX = gameObject.transform.position.x;
        var oldZ = gameObject.transform.position.z;

        var horizontal = Input.GetAxis("Horizontal");
        var newX = oldX += horizontal * PlayerSpeed;

        var vertical = Input.GetAxis("Vertical");
        var newZ = oldZ += vertical * PlayerSpeed;
        gameObject.transform.position = new Vector3(newX, o.y, newZ);
    }
}